extern crate serde_json;

use std::error::Error;
use std::collections::HashMap;
use std::fs::File;
use std::sync::Arc;
use serde_json::{Value};

pub enum DefinitionType {
    Number,
    Str,
    Object
}

pub struct DynamicTypeDefinition {
    pub name: String,
    pub def_type: DefinitionType,
    pub store_index: usize
}

impl DynamicTypeDefinition {
    pub fn build(name: String, def_type: DefinitionType, store_index: usize) -> DynamicTypeDefinition {
        DynamicTypeDefinition {
            name,
            def_type,
            store_index
        }
    }

    pub fn build_str_def(name: &str, store_index: usize) -> DynamicTypeDefinition {
        DynamicTypeDefinition {
            name: name.to_string(),
            store_index,
            def_type: DefinitionType::Str
        }
    }
}

pub struct DynamicTypeVal {
    // Hopefully can change this to use generics
    definition: Arc<DynamicTypeDefinition>,
    num_val: Option<i64>,
    str_val: Option<String>,
    obj_val: Option<DynamicTypeInstance>
}


impl DynamicTypeVal {

    pub fn build_for_type(definition: Arc<DynamicTypeDefinition>) -> DynamicTypeVal {
        DynamicTypeVal {
            definition,
            str_val: None,
            num_val: None,
            obj_val: None
        }
    }
}

pub struct DynamicTypeInstance {
    // At some point it would be good to add array like notation to the type
    // so the fields can be access as
    // type["field_name"]
    pub params: HashMap<String, DynamicTypeVal>
}

impl DynamicTypeInstance {

    pub fn new() -> DynamicTypeInstance {
        DynamicTypeInstance {
            params: HashMap::new()
        }
    }

    pub fn set_str_val(&mut self, name: &str, value:String) {
        let dyn_type_val = self.params.get_mut(name);
        match dyn_type_val {
            Some(val) => {
                val.str_val = Some(value)
            },
            None => {} //Possibly panic
        }
    }

    pub fn set_num_val(&mut self, name: &str, value: i64) {
        let dyn_type_val = self.params.get_mut(name);
        match dyn_type_val {
            Some(val) => {
                val.num_val = Some(value)
            },
            None => {}
        }
    }

    pub fn get_number_val(&self, name: &str) -> Option<&i64> {
        // Gets the named numeric parameter
        let dyn_type_val = self.params.get(name);
        match dyn_type_val {
            Some(ref val) => {
                match val.definition.def_type {
                    DefinitionType::Number => { 
                        match val.num_val {
                            Some(ref i) => Some(&i),
                            None => None
                        }    
                    },
                    _ => None
                }
            },
            None => None
        }
    }

    pub fn get_string_val(&self, name: &str) -> Option<&str> {
        let dyn_type_val = self.params.get(name);
        match dyn_type_val {
            Some(ref val) => {
                match val.definition.def_type {
                    DefinitionType::Str => {
                        match val.str_val {
                            Some(ref s) => Some(s.as_str()),
                            None => None
                        }
                    },
                    _ => None
                }
            },
            None => None
        }
    }

    pub fn as_json(&self) -> String {
        // A very simple "To Json" implementation
        let mut out = String::from("{");
        
        let mut index = 0;
        let len = self.params.len();
        for key in self.params.keys() {
            let val_opt = self.params.get(key);
            match val_opt {
                Some(val) => {
                    out.push_str(format!("\"{}\": ", key).as_str());
                    match val.definition.def_type {
                        DefinitionType::Str => { 
                            // Need to escape the string properly
                            match val.str_val {
                                Some(ref s) => { out.push_str(format!("\"{}\"", s).as_str()); },
                                None => { out.push_str("null"); }
                            }                    
                        },
                        DefinitionType::Number => { 
                            match val.num_val {
                                Some(i) => {out.push_str(format!("{}", i).as_str()); },
                                None => { out.push_str("null"); }
                            }
                        },
                        DefinitionType::Object => { 
                            match val.obj_val {
                                Some(ref obj) => {
                                    out.push_str(format!("{}", obj.as_json()).as_str());
                                },
                                None => { out.push_str("null") }
                            }
                        }
                    }
                    index = index + 1;
                    if index < len {
                        out.push_str(", ");
                    }
                },
                None => {} // Not doing anything for empyt values. Consider adding "param_name: null"
            }
        }

        out.push('}');
        return out
    }
}

pub struct DynamicType {
    name: String,
    // The field definitions are shared by all instances across multiple threads,
    // hence the DynamicTypeDefinition is stored in an Arc
    fields: HashMap<String, Arc<DynamicTypeDefinition>>
}

impl DynamicType {
    pub fn new(name: &str) -> DynamicType {
        DynamicType{
            name: String::from(name),
            fields: HashMap::new()
        }
    }

    pub fn add_string_field(&mut self, name: &str) {
        self.fields.insert(String::from(name), Arc::new(DynamicTypeDefinition::build(String::from(name), DefinitionType::Str, 0)));
    }

    pub fn add_number_field(&mut self, name: &str) {
        self.fields.insert(String::from(name), Arc::new(DynamicTypeDefinition::build(String::from(name), DefinitionType::Number, 0)));
    }

    pub fn as_instance(&self) -> DynamicTypeInstance {
        let mut instance = DynamicTypeInstance::new();

        // Add all the possible fields as uninitialised
        for key in self.fields.keys() {
            let def = self.fields.get(key);
            match def {
                Some(d) => {
                    instance.params.insert(key.clone(), DynamicTypeVal::build_for_type(d.clone()));
                },
                None => {}
            }
        }
        instance
    }
}

pub struct DynamicTypeFactory {
    types: HashMap<String, DynamicType>
}

impl DynamicTypeFactory {
    pub fn new() -> DynamicTypeFactory {
        DynamicTypeFactory {
            types: HashMap::new()
        }
    }

    pub fn from_file(file_name: &str) -> Result<DynamicTypeFactory, Box<Error>> {
        let mut factory = DynamicTypeFactory::new();
        // Use serde to deserialize the json, but do it as raw Value and build our own objects from there.
        let f = File::open(file_name)?;

        let raw_fact:Value = serde_json::from_reader(f)?;

        // The json looks like 
        // {
        //    "types":[{
        //           "name": type name,
        //           "fields": [DynamicTypeDefinition] 
        //    }]
        // }
        if raw_fact["types"].is_array() {
            let types_array = raw_fact["types"].as_array().unwrap();
            for type_def in types_array {
                println!("Got type {}", type_def["name"]);
                // Create the new type
                let mut new_type = DynamicType::new(type_def["name"].as_str().unwrap());
                // Now add all the fields
                if type_def["fields"].is_array() {
                    let fields = type_def["fields"].as_array().unwrap();
                    for field in fields {
                        let field_type = field["type"].as_str().unwrap();
                        let field_name = field["name"].as_str().unwrap();
                        match field_type.as_ref() {
                            // These are the enum types. Should be constants somewhere
                            "String" => { new_type.add_string_field(field_name); },
                            "Number" => { new_type.add_number_field(field_name); } // Maybe allow range specifiers as well
                            _ => {}
                        }
                    }
                }
                // Nothing has gone wrong parsing the fields. Add the type
                factory.types.insert(new_type.name.clone(), new_type);
            }
        }
        Ok(factory)
    }

    pub fn get_type_instance(&self, type_name: &str) -> Option<DynamicTypeInstance> {
        self.types.get(type_name)
            .map_or(None, |dt| Some(dt.as_instance()))
    }
}
