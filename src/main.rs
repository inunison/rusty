#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

extern crate rocket;
mod mock_store;
mod dynamic_types;

use std::sync::{Mutex, RwLock};
use std::collections::HashMap;
use mock_store::MockTable;
use rocket::State;
use dynamic_types::{DynamicTypeFactory, DynamicTypeInstance, DefinitionType };

type DBMap = Mutex<HashMap<String, MockTable>>;

#[get("/")]
fn index() -> &'static str {
    "Hello from rustyopoly"
}

#[get("/<type_name>/<id>", format="application/json")]
fn type_handler(type_name: String, id: i32, database: State<DBMap>, factory: State<RwLock<DynamicTypeFactory>>) -> String {
    match type_name.as_str() {
        "user" => {
            let mut user = get_user(id, database, factory);
            match user {
                Some(mut u) => format!("{}", u.as_json()),
                None => format!("User {} not found", id)
            }
        },
        _ => format!("The type {} is unsupported", type_name)
    }
}

fn get_user(id: i32, database: State<DBMap>, types_factory: State<RwLock<DynamicTypeFactory>>) -> Option<DynamicTypeInstance> {
    // Could just use unwrap here so the line looks like:
    // let mut map = database.read().unwrap()
    // But apparantly that's discouraged for production and checking for Ok/Err is better.
    

    // get the type out of the factory before acquiring the db lock to prevent deadlocks
    let mut instance: Option<DynamicTypeInstance> = None;
    {
        let fact_lock = types_factory.read();
        match fact_lock {
            Ok(factory) => {
                instance = factory.get_type_instance("user");
            },
            Err(_) => {}
        }
    } // factory should be unlocked here

    // Now read the database and populate the instance
    match instance {
        Some(mut dyn_type) => {
            let map_res = database.lock();
            match map_res {
                Ok(mut map) => {
                    if map.contains_key("user") {
                        let mut tbl = map.get_mut("user").unwrap();
                        let user_row = tbl.get_row(id as usize);
                        match user_row {
                            Some(row) => {
                                println!("Username = {}", &row[0]);
                                // Build a DynamicType that looks like a User
                                
                                // Create the type definitions, this would normally be done reading config
                                // at start up, and then some factory object to get the type def from the name
                                // let mut dyn_type = DynamicTypeInstance::new()
                                //     .with_param("username", DefinitionType::Str, 0)
                                //     .with_param("given_name", DefinitionType::Str, 1)
                                //     .with_param("family_name", DefinitionType::Str, 2)
                                //     .with_param("email", DefinitionType::Str, 3);

                                // Set the values by reading the store. In the real world this would be done
                                // by reading a DB and using field names on the type definitions to read values
                                // from the returned data 
                                dyn_type.set_str_val("username", row[0].clone());
                                dyn_type.set_str_val("given_name", row[1].clone());
                                dyn_type.set_str_val("family_name", row[2].clone());
                                dyn_type.set_str_val("email", row[3].clone());
                                dyn_type.set_num_val("age", row[4].parse::<i64>().unwrap());

                                return Some(dyn_type)
                            },
                            None => {
                                println!("User {} not found", id);
                                // Panic or return None?
                            }
                        }
                    } else {
                        println!("No user table");
                        // Panic or return None
                    }
                },
                Err(_map) => {
                    println!("Unable to acquire db lock");
                    // Panic or return None
                }
            }
        },
        None => {
            println!("Could not instantiate type.");
            // Panic!
        }
    }
    None
}

fn main() {
    // mock generic data store
    let mut user_data: MockTable = MockTable::new();
    user_data.add_row(
        vec![
            "luke_skywalker".to_string(), 
            "Luke".to_string(), 
            "Skywalker".to_string(), 
            "luke@tatooine.com".to_string(),
            "18".to_string()]);

    user_data.add_row(
        vec![
            "han_solo".to_string(), 
            "Han".to_string(), 
            "Solo".to_string(), 
            "han_and_chewie@falcon.com".to_string(),
            "30".to_string()]);

    user_data.add_row(vec![
        "darth_vader".to_string(), 
        "Darth".to_string(), 
        "Vader".to_string(), 
        "i_am_evil@darkside.com".to_string(),
        "50".to_string()]);

    let mut db_map: HashMap<String, MockTable> = HashMap::new();
    db_map.insert("user".to_string(), user_data);

    
    // Create the dynamic type factory from the types json file
    let type_factory = DynamicTypeFactory::from_file("types.json");

    // path bindings
    rocket::ignite()
        .manage(Mutex::new(db_map))
        .manage(RwLock::new(type_factory.unwrap()))
        .mount("/", routes![index, type_handler])
        .launch();
}