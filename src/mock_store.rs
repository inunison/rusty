pub type TableRow = Vec<String>;

// This module is a mock data store, replicating something like a relational database
pub struct MockTable {
    data: Vec<TableRow>
}

impl MockTable {
    pub fn new() -> MockTable {
        MockTable {
            data: Vec::new()
        }
    }

    pub fn add_row(&mut self, row: TableRow) {
        self.data.push(row);
    }

    pub fn get_row(&mut self, index: usize) -> Option<&TableRow> {
        self.data.get(index)
    }
}

