# README #

An experimental web server using Rust and Rocket. The purpose is to experiment with ways of serving generic types not known at compile time

## Getting started ##

### Install Rust ###
Follow the instructions at https://www.rust-lang.org/en-US/install.html in order to get Rust installed on your machine

### Override Rust version for Rocket ###
Rocket requires an unstable version of Rust to run. This can be done by using an override for the project so the version of Rust used by this project alone will be the one supported by Rocket.
The instructions are here. https://rocket.rs/guide/getting-started/#installing-rust

### Starting rustyopoly ###
In the root of the project run
```
cargo run
```

It should then download the dependencies and start the server

Navigate to `http://localhost:8000` and you should see the message `Hello from rustyopoly`

## Aims ##
The aims are to enable the web server to read in Json files that define the Types and REST API the server supports. The type definition will specify the name of the type to use as the url, 
how to extract the data from the store and how to present that type as a Json response to a REST call.

For instance, if a type User is defined with the url name `user`, then navigating to `http://localhost:8000/user/1` will retrieve the data for User with id 1 from the store and return it as 
User Json in the response.

